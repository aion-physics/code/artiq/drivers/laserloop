from sipyco.pc_rpc import Client
from simple_pid import PID
from threading import Thread
import time

import logging

logger = logging.getLogger(__name__)

kp, kd, ki = -7.0, 0.0, -0.6


class LaserLock:

    def set_setpoint_nm(self, wavelength):
        """Sets the setpoint to a given wavelength."""
        logger.info('New laser setpoint={}nm'.format(wavelength))
        self.setpoint = wavelength

    def lock(self):
        """Enables the lock."""
        logger.info('Laser lock enabled.')
        #self.enable = False
        # wait for loop to not be updating (prevent race condition)
        #time.sleep(0.5)
        #self.voltage = self.laser.get("laser1:dl:pc:voltage-set")
        #self.pid.reset()  # clear accumulator
        self.enable = True

    def unlock(self):
        """Disables the lock."""
        logger.info('Laser lock disabled.')
        self.enable = False

    def ping(self):
        return True

    def __init__(self, wavemeter, laser, piezo_limits):
        self.laser_addr = laser
        self.wavemeter_addr = wavemeter
        self.piezo_limits = piezo_limits
        self.last_logged = time.time()

    async def __aenter__(self):
        logger.info("Connecting to laser artiq device.")
        try:
            self.laser = Client(
                self.laser_addr['ip'], self.laser_addr['port'], "laser", timeout=3)
        except:
            logger.error("Exception occured while connecting to laser.")
            raise
        logger.info("Connecting to wavemeter artiq device.")
        try:
            self.wavemeter = Client(
                self.wavemeter_addr['ip'], self.wavemeter_addr['port'], "wavemeter", timeout=3)
        except:
            logger.error("Exception occured while connecting to wavemeter.")
            raise
        logger.info("Starting laser control loop thread.")
        self.thread = Thread(target=self._control_loop)
        self.thread.start()
        return self

    def set_loop_limits(self, limits):
        """Sets the output limits for the PID loop"""
        logger.info("New output limits for loop: {}".format(limits))
        self.piezo_limits = limits
        self.laser.set_piezo_voltage_limits(
            self.piezo_limits[0], self.piezo_limits[1], diode_num=self.laser_addr["diode_number"])
        self.pid.output_limits = (self.piezo_limits[0], self.piezo_limits[1])

    def set_loop_parameters(self, pid_constants):
        """Sets the PID parameters for the loop."""
        logger.info("Setting loop parameters: {}".format(pid_constants))
        self.pid.tunings = pid_constants

    def _startup(self):
        logger.info("Thread for PID loop starting.")
        self.pid = PID(kp, kd, ki)
        self.set_loop_limits(self.piezo_limits)
        self.set_setpoint_nm(460.862015)  # nm
        self.pid.sample_time = 0.1
        logger.info('Setting piezo voltage limits')
        self.enable = False
        logger.info('Loop set to disabled, enable it with lock().')
        self.voltage = self.laser.get("laser{}:dl:pc:voltage-set".format(self.laser_addr["diode_number"]))
        logger.info('Starting voltage={}'.format(self.voltage))
        self.running = True

    def _control_loop(self):
        self._startup()
        while (self.running):
            enter_time = time.time()
            if self.enable:
                self.pid.setpoint = self.setpoint
                wavelength = self.wavemeter.get_wavelength(channel=self.wavemeter_addr["channel"])
                new_voltage = self.pid(wavelength)
                try:
                    self.laser.set_piezo_voltage(new_voltage,diode_num=self.laser_addr["diode_number"])
                    self.voltage = new_voltage
                except Exception as err:
                    logger.error("Exception occurred while setting laser voltage to {0} (pid components={1}): {2}".format(
                        new_voltage, self.pid.components, err))
                if time.time() - self.last_logged > 2:
                    self.last_logged = time.time()
                    #commented out to prevent spam filling up the artiq log
                    #logger.info("testing PID output voltage={}, components={}".format(
                    #    self.voltage, self.pid.components))
            elapsed = time.time() - enter_time
            wait_duration = self.pid.sample_time - elapsed
            if wait_duration > 0:
                time.sleep(wait_duration)

    async def __aexit__(self):
        self.laser.close_rpc()
        self.wavemeter.close_rpc()
        self.running = False
        self.thread.join()
