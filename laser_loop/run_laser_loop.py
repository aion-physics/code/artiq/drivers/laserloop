import argparse
import logging
import sys
import asyncio

from laser_loop.loop import LaserLock

from sipyco.pc_rpc import Server
from sipyco import common_args

logger = logging.getLogger(__name__)

def get_argparser():
   parser = argparse.ArgumentParser(
      description="""ARTIQ software-based laser lock.""")
   parser.add_argument("--wavemeter-ip", default=None, required=True, help="Wavemeter address.")
   parser.add_argument("--wavemeter-port", default=None, required=True, help="Wavemeter port.")
   parser.add_argument("--wavemeter-channel", default=None, type=int, choices=list(range(1,9)), required=True, help="Wavemeter channel")
   parser.add_argument("--laser-ip", default=None, required=True, help="Laser ip address.")
   parser.add_argument("--laser-port", default=None, required=True, help="Laser port.")
   parser.add_argument("--laser-diode-number", default=None, type=int, choices=[1,2], required=True, help="Laser diode number (1 or 2)")
   common_args.simple_network_args(parser, 1337)
   common_args.verbosity_args(parser)
   return parser

def main():
   args = get_argparser().parse_args()
   common_args.init_logger_from_args(args)

   # if args.wavemeter_ip is None:
   #    logger.error("You need to supply a --wavemeter-ip argument. Use --help for more information.")
   #    sys.exit(1)

   # if args.wavemeter_port is None:
   #    logger.error("You need to supply a --wavemeter-port argument. Use --help for more information.")
   #    sys.exit(1)

   # if args.laser_ip is None:
   #    logger.error("You need to supply a --laser-ip argument. Use --help for more information.")
   #    sys.exit(1)

   # if args.laser_port is None:
   #    logger.error("You need to supply a --laser-port argument. Use --help for more information.")
   #    sys.exit(1)

   async def run():
      async with LaserLock(
         { "ip": args.wavemeter_ip, "port": args.wavemeter_port, "channel": args.wavemeter_channel },
         { "ip": args.laser_ip, "port": args.laser_port, "diode_number": args.laser_diode_number },
         [ 30.0, 33.0 ]
      ) as laserlock:
         logger.info('Laser lock created, starting server.')
         server = Server({"lock": laserlock}, None, True)
         await server.start(common_args.bind_address_from_args(args), args.port)
         try:
            await server.wait_terminate()
         finally:
            await server.stop()

   loop = asyncio.get_event_loop()
   try:
      loop.run_until_complete(run())
   except KeyboardInterrupt:
      pass
   finally:
      loop.close()

   if __name__ == "__main__":
      main()
