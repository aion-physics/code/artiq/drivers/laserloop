{
  description = "ARTIQ controller for a simple software laser loop";

  inputs.nixpkgs.url = github:NixOS/nixpkgs/nixos-21.11;
  inputs.sipyco.url = github:m-labs/sipyco;  
  inputs.sipyco.inputs.nixpkgs.follows = "nixpkgs";

  outputs = { self, nixpkgs, sipyco }:
    let
      pkgs = import nixpkgs { system = "x86_64-linux"; };
    in rec {
      packages.x86_64-linux.simple-pid = pkgs.python3Packages.buildPythonPackage rec {
        pname = "simple-pid";
        version = "1.0.1";
        src = pkgs.python3Packages.fetchPypi {
          pname = "simple-pid";
          inherit version;
          sha256 = "60d6b3a7c3f19734d33bbc5938293c4924d7cfea74176def03306057b3f99524";
        };
        doCheck = false;
        propagatedBuildInputs =  with pkgs.python3Packages; [  ];
      };  

      packages.x86_64-linux = {
        laser-loop = pkgs.python3Packages.buildPythonPackage {
          pname = "laser-loop";
          version = "0.1";
          src = self;
          propagatedBuildInputs = [ sipyco.packages.x86_64-linux.sipyco packages.x86_64-linux.simple-pid ];
        };
      };

      defaultPackage.x86_64-linux = pkgs.python3.withPackages(ps: [ packages.x86_64-linux.laser-loop ]);

      devShell.x86_64-linux = pkgs.mkShell {
        name = "laser-loop-dev-shell";
        buildInputs = [
          (pkgs.python3.withPackages(ps: [ sipyco.packages.x86_64-linux.sipyco packages.x86_64-linux.laser-loop ]))
        ];
      };
    };
}
