#!/usr/bin/env python3

import sys
from setuptools import setup
from setuptools import find_packages

setup(
   name="laser-loop",
   version="0.1",
   description="ARTIQ controller for a software-based laser loop",
   author="Elliot Bentine",
   author_email="elliot.bentine@physics.ox.ac.uk",
   url="https://gitlab.com/foot-group/laserloop",
   download_url="https://gitlab.com/foot-group/laserloop",
   packages=find_packages(),
   install_requires=["simple-pid"],
   entry_points={
      "console_scripts": [
         "run_laser_loop = laser_loop.run_laser_loop:main",
      ]
   },
   license="LGPLv3+",
)
